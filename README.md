# VWS ui applications

The main goal of this project is to share common code/components/constants between VWS ui applications.

## How does it work ?

### Exports

Every services or component that you want to use in another project should be exported in *public_api.ts* 

## What's inside ?

#### Shared modules, ex: *navbar*

The shared modules are groups of components, which will be reused in several VWS applications.

Regarding the code architecture of the first module, navbar is composed of:  
  * a main component, which acts like a container
     * its template contains the child components (see navbar.component.html)
     * the component's class contains the business logic of all the module (see navbar.component.ts)
  * child components
     * these are just presentational components, the data is passed down through these components  

#### *api*

Services created to communicate with our REST API back-end

#### *constant*

Define here the const declarations that we should not be able to re-assign (ex: VWS user's roles)

#### *model*

Define contracts for the "shape" of your values (ex: a *User* interface with firstname, lastname, etc.)

#### *util*

Utility services, these files contain reusable functions and services (ex: DateService)

#### *assets*

Some images and .less files

## How to test current core-lib dev into dependent projects

Make the dist folder a global symlink with

``` 
cd dist
npm link
``` 

Then, inside the dependent project (report analysis v2 ui, for example)

``` 
npm link @smartviser/core-lib
``` 

This is usefull to test a newly developped feature, when the dependencies didn't change.
In case of a dependency change, increase the version, set it to dev and install in via npm install in the dependent project instead of linking.
#### NOTE: as of now it is important to install the dependancies with node 12.14.1 installed locally, to easily switch between versions of node, take a look at [NVM](https://github.com/nvm-sh/nvm)

## How to build ans publish the library to npm

To build the library (without Ivy or NGCC, that's the prod case)

``` 
npm run package:prod
``` 

After that you need to publish the built libary to npm (you need a npm account) go to the dist foder and run :

``` 
cd dist
npm publish
``` 

## How to use this lib in another project ?

Add <@smartviser/core-lib : version> in your package.json.

Add this json in the asset array of your angular.json file

```
              {
                "glob": "**/*",
                "input": "./node_modules/@smartviser/core-lib/src",
                "output": "core/"
              },
``` 

You can use and display a shared module in your project by importing its main component.  
To import component : <import { NavbarModule } from '@smartviser/core-lib';>
The prefix used here is "vws".  
For example:

``` 
<vws-navbar [parentService]="$service"></vws-navbar>
``` 

## Roles handling

**Roles:** 
```
{
    ADMIN: 'admin',
    ADMIN_SV: 'adminSV',
    ADMIN_SYSTEM: 'adminSystem',
    DEMO: 'demo',
    USER: 'user',
    USER_SV: 'userSV',
}

#core-lib/projects/vws-core-lib/src/lib/constant/role.constant.ts
```
 
**Groups :**
```
{
    CUSTOMER_ROLES: ['admin', 'user'],
    SV_ADMINS: ['adminSystem', 'adminSV'],
    ADMINS: ['adminSystem', 'adminSV', 'admin'],
    SV_ROLES: ['adminSystem', 'adminSV', 'userSV']
}

#core-lib/projects/vws-core-lib/src/lib/constant/role.constant.ts
```

### only for ADMIN_SYSTEM (superadmin):

manage additional fields during add / update template (V1) :

* ID
* Data function
* position
* icon
* classes
* compareclasses
