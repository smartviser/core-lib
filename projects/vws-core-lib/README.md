# VWS ui applications

The main goal of this project is to share common code/components/constants between VWS ui applications.

## How does it work ?

### Exports

Every services or component that you want to use in another project should be exported in *public_api.ts* 

## What's inside ?

#### Shared modules, ex: *navbar*

The shared modules are groups of components, which will be reused in several VWS applications.

Regarding the code architecture of the first module, navbar is composed of:  
  * a main component, which acts like a container
     * its template contains the child components (see navbar.component.html)
     * the component's class contains the business logic of all the module (see navbar.component.ts)
  * child components
     * these are just presentational components, the data is passed down through these components  

#### *api*

Services created to communicate with our REST API back-end

#### *constant*

Define here the const declarations that we should not be able to re-assign (ex: VWS user's roles)

#### *model*

Define contracts for the "shape" of your values (ex: a *User* interface with firstname, lastname, etc.)

#### *util*

Utility services, these files contain reusable functions and services (ex: DateService)

#### *assets*

Some images and .less files

## How to use this lib in another project ?

Add <@smartviser/core-lib : version> in your package.json.

You can use and display a shared module in your project by importing its main component.  
To import component : <import { NavbarModule } from '@smartviser/core-lib';>
The prefix used here is "vws".  
For example:

``` 
<vws-navbar [parentService]="$service"></vws-navbar>
``` 
