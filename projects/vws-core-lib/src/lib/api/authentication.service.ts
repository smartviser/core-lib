import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {User} from '../model/user.interface';
import {CookieService} from '../util/cookie.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    
    private readonly token: string;

    constructor(private http: HttpClient, private cookieService: CookieService) {
        const userConnected: User = this.cookieService.getUserConnected() ;
        this.token = userConnected ? userConnected.token : null;
    }

    getToken(): string {
        return this.token;
    }
}
