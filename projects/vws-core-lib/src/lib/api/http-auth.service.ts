import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {AuthenticationService} from './authentication.service';
import {HttpConstants} from '../constant/http.constants';

@Injectable({
    providedIn: 'root'
})
export class HttpAuthService {
    constructor(private readonly http: HttpClient, private readonly authenticationService: AuthenticationService) {}

    public get(url: string, params?: any): Observable<any> {
        return this.http.get(url, {headers: this.generateHeaders(), params});
    }

    public post(url: string, body: any): Observable<any> {
        return this.http.post(url, body, {headers: this.generateHeaders()});
    }

    public put(url: string, body: any): Observable<any> {
        return this.http.put(url, body, {headers: this.generateHeaders()});
    }

    public delete(url: string): Observable<any> {
        return this.http.delete(url, {headers: this.generateHeaders()});
    }

    private generateHeaders(): HttpHeaders {
        const headersConfig: string | { [name: string]: string | string[] } = {};
        headersConfig[HttpConstants.AUTH_TOKEN] = this.authenticationService.getToken() || '';
        return new HttpHeaders(headersConfig);
    }
}
