import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpAuthService} from './http-auth.service';

import {EnvironmentConfig} from '../environment/environment';
import { VWSService } from '../model/vws-service.interface';

@Injectable({
    providedIn: 'root'
})
export class VWSServicesService {
    constructor(
        private httpAuthService: HttpAuthService,
        private environmentConfig: EnvironmentConfig
    ) {
    }

    findUserServices = (): Observable<VWSService[]> => this.httpAuthService.get(`${this.environmentConfig.base_url}/users/services`, {});
    
}
