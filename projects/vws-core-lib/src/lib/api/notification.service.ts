import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpAuthService} from './http-auth.service';

import {EnvironmentConfig} from '../environment/environment';
import {ApiConstants} from '../constant/api.constants';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private readonly commonBaseUrl: string;
    
    constructor(
        private httpAuthService: HttpAuthService,
        private environmentConfig: EnvironmentConfig
    ) {
        this.commonBaseUrl = `${this.environmentConfig.commonBaseUrl}${ApiConstants.core.notifications}`;
    }

    list(pageNumber: number, step: number): Observable<any> {
        return this.httpAuthService.get(`${this.commonBaseUrl}`, {
            pageNumber: pageNumber.toString(),
            step: step.toString()
        });
    }

    removeById(id: number): Observable<any> {
        return this.httpAuthService.delete(`${this.commonBaseUrl}/${id}`);
    }

    remove(): Observable<any> {
        return this.httpAuthService.delete(`${this.commonBaseUrl}`);
    }
}
