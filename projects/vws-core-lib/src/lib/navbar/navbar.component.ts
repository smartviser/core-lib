import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {User} from '../model/user.interface';
import {ComponentService} from '../util/component.service';
import {CookieService} from '../util/cookie.service';
import { RoleConstants } from '../constant/role.constant';
import { VwsEnvironment } from '../model/vws-environment.interface';
import { MenuItem } from '../model/menu-item.interface';
import { SecurityService } from '../util/security.service';

@Component({
    selector: 'vws-navbar[parentService]',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {
    @Input() parentService: VwsEnvironment;
    @Input() sticked: boolean = false;
    @Input() subtitle: string;
    @Input() subtitleChips: boolean = false;
    @Input() menuItems: MenuItem[] = [];
    @Input() isDashboard: boolean = false;
    @Input() isUpload: boolean = false;
    @Input() showSideNavToogle?: boolean = false;
    @Output() openedSideNav: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() goToHome: EventEmitter<void> = new EventEmitter<void>();

    // TODO refactor to remove all these "any"
    userName$: string = null;
    userNameInitial$: string = null;
    vwsServices$: VwsEnvironment[] = [];
    notifications$: any = [];
    totalNotifications$: any = null;
    userConnected: User = null;

    private readonly pageNumber: number = 0;
    private readonly step: number = 5;

    constructor(private cookieService: CookieService,
                private componentService: ComponentService,
                private securityService: SecurityService) {
    }

    ngOnInit(): void {
        this.componentService.checkRequiredParameter(this.parentService, 'parentService');
        this.userConnected = this.cookieService.getUserConnected() ;
        if(this.userConnected){
            this.securityService.getUserService(this.parentService).subscribe({
                next: (services: VwsEnvironment[]): void => {
                    this.vwsServices$ = services;
                }, 
                error: (): void => {
                    console.error('Could not get your VWS services..');
                }});
        }
    }

    isAdmin(): boolean {
        const userConnected: User = this.cookieService.getUserConnected() ;
        return RoleConstants.GROUPS.ADMINS.includes(userConnected.roles);
    }

    /**
     * Emit output to open or close sidenav
     */
    public openSideNav(isOpen: boolean): void {
        this.openedSideNav.emit(isOpen);
    } 

    public goToHomeClick(): void {
        this.goToHome.emit();
    } 
  
}
