import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { MatChipsModule } from '@angular/material/chips'; 
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button'; 
import { MatMenuModule } from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDividerModule} from '@angular/material/divider';

import { NavbarComponent } from './navbar.component';
import { NavbarAdminComponent } from './components/navbar-admin/navbar-admin.component';
import { NavbarAppsMenuComponent } from './components/navbar-apps-menu/navbar-apps-menu.component';
import { NavbarHeaderComponent } from './components/navbar-header/navbar-header.component';
import { NavbarNotificationComponent } from './components/navbar-notification/navbar-notification.component';
import { NavbarUserMenuComponent } from './components/navbar-user-menu/navbar-user-menu.component';
import { NavbarUploadComponent } from './components/navbar-upload/navbar-upload.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {EnvironmentConfig} from '../environment/environment';
import { NavbarMenuComponent } from './components/navbar-menu/navbar-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        NavbarComponent,
        NavbarAdminComponent,
        NavbarAppsMenuComponent,
        NavbarHeaderComponent,
        NavbarNotificationComponent,
        NavbarUserMenuComponent,
        NavbarUploadComponent,
        NavbarMenuComponent
    ],
    imports: [
        BrowserModule,
        MatChipsModule,
        MatDividerModule,
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatToolbarModule,
        MatGridListModule,
        RouterModule
    ],
    providers: [],
    bootstrap: [NavbarComponent],
    exports: [NavbarComponent]
})
export class NavbarModule { 
    static forRoot(configuration: EnvironmentConfig): ModuleWithProviders<NavbarModule> {
        return {
            ngModule: NavbarModule,
            providers: [{provide: EnvironmentConfig, useValue: configuration}]
        };
    }
}
