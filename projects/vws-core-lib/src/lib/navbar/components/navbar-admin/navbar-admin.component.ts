import { Component } from '@angular/core';

@Component({
    selector: 'vws-navbar-admin',
    templateUrl: './navbar-admin.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-admin.component.less'
    ]
})
export class NavbarAdminComponent {
    adminUrl: string = '/admin';
}
