import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ComponentService} from '../../../util/component.service';

@Component({
    selector: 'vws-navbar-header',
    templateUrl: './navbar-header.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-header.component.less'
    ]
})
export class NavbarHeaderComponent implements OnInit {
    @Input() serviceName: string;
    @Input() serviceUrl: string;
    @Input() showSideNavToogle: boolean;
    @Output() openedSideNav: EventEmitter<boolean> = new EventEmitter();
    @Output() goToHome: EventEmitter<void> = new EventEmitter();

    isOpen: boolean;

    constructor(private componentService: ComponentService) {
        this.isOpen = false;
        this.openedSideNav.emit(this.isOpen);
    }

    ngOnInit(): void {
        this.componentService.checkRequiredParameter(this.serviceName, 'serviceName');
    }

    /**
     * Emit output to open or close sidenav
     */
    public openSideNav(): void {
        this.isOpen = !this.isOpen;
        this.openedSideNav.emit(this.isOpen);
    } 

    /**
     * Emit output to go back to the home page
     */
    public goToHomeClick(): void {
        this.goToHome.emit();
    }
}
