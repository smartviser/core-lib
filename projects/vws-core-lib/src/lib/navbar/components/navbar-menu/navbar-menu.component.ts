import { Component, Input } from '@angular/core';
import { MenuItem } from '../../../model/menu-item.interface';

@Component({
    selector: 'vws-navbar-menu',
    templateUrl: './navbar-menu.component.html',
    styleUrls: ['./navbar-menu.component.less']
})
export class NavbarMenuComponent {
    @Input() menuItems: MenuItem[] = [];
}
