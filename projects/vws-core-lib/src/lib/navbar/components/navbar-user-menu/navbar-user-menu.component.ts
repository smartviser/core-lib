import {Component, Input, OnInit} from '@angular/core';
import { User } from '../../../model/user.interface';
import {CookieService} from '../../../util/cookie.service';
import { LocalStorageService } from '../../../util/local-storage.service';

@Component({
    selector: 'vws-navbar-user-menu',
    templateUrl: './navbar-user-menu.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-user-menu.component.less'
    ]
})
export class NavbarUserMenuComponent implements OnInit{

    @Input() userConnected: User;
    userName: string = '';
    userNameInitial: string = '';
    email: string = '';
    userMenu: any[] = [];

    constructor(private cookieService: CookieService, private localSotrageService: LocalStorageService){}

    ngOnInit(): void {
        this.userName = `${this.userConnected.name} ${this.userConnected.surname}`;
        this.userNameInitial = `${this.userConnected.name[0]}${this.userConnected.surname[0]}`;
        this.email = `${this.userConnected.email}`;
        this.userMenu = [
            /* disabled for now since we can't make direct link to my account*/
            //{ icon: 'person', text: 'My account', action: this.account },
            { icon: 'logout', text: 'Log out', action: this.disconnect }
        ];
    }

    account = (): string => window.location.href = '/account';

    disconnect = (): void => {
        this.cookieService.disconnectUser();
        this.localSotrageService.removeLocalStorageItem('ls.user_services');
        window.location.href = '/login';
    };
}
