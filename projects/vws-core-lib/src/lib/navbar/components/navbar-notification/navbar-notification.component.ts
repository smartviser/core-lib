import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ComponentService} from '../../../util/component.service';

@Component({
    selector: 'vws-navbar-notification',
    templateUrl: './navbar-notification.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-notification.component.less'
    ]
})
export class NavbarNotificationComponent implements OnInit {
    @Input() notifications: any;
    @Input() totalNotifications: any;
    @Output() refresh: EventEmitter<any> = new EventEmitter();
    @Output() discard: EventEmitter<any> = new EventEmitter();
    @Output() discardById: EventEmitter<any> = new EventEmitter();

    constructor(private componentService: ComponentService) {
    }

    ngOnInit(): void {
        this.componentService.checkRequiredParameter(this.notifications, 'notifications');
    }

    onDiscard(notificationId: number): void {
        this.discardById.emit(notificationId);
    }

    onRefresh(): void {
        this.refresh.emit();
    }

    onDiscardAll(): void {
        this.discard.emit();
    }
}
