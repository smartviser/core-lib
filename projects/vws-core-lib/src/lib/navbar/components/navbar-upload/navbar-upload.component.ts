import { Component } from '@angular/core';

@Component({
    selector: 'vws-navbar-upload',
    templateUrl: './navbar-upload.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-upload.component.less'
    ]
})
export class NavbarUploadComponent {
    uploadUrl: string = '/analytics_studio/upload';
}
