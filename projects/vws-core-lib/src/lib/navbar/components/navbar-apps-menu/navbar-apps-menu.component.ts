import {Component, Input} from '@angular/core';

@Component({
    selector: 'vws-navbar-apps-menu',
    templateUrl: './navbar-apps-menu.component.html',
    styleUrls: [
        '../../navbar.component.less',
        './navbar-apps-menu.component.less'
    ]
})
export class NavbarAppsMenuComponent {
    @Input() vwsServices: [];
}
