import { VwsEnvironment } from '../model/vws-environment.interface';


export const VWS_REPORT_ANALYSIS_V2: VwsEnvironment = {
    url: '/analytics_studio',
    id: 'vws_report_analysis',
    name: 'Analytics Studio',
    icon: 'query_stats',
    order: 1,
    target: '_self'
};

export const VWS_REMOTE_CONTROL: VwsEnvironment = {
    url: '/remote-control',
    id: 'vws_remote_control',
    name: 'Remote control',
    icon: 'settings_remote',
    order: 2,
    target: '_self'
};

export const VWS_DOCUMENTATION: VwsEnvironment = {
    url: '/documentation',
    id: 'vws_documentation',
    name: 'Documentation',
    icon: 'description',
    order: 3,
    target: '_blank'
};

export const VWS_SUPPORT: VwsEnvironment = {
    url: 'https://smartviser.atlassian.net/servicedesk/customer/portals',
    id: 'vws_support',
    name: 'Support',
    icon: 'support_agent',
    order: 4,
    target: '_blank'
};

export const VWS_ADMIN: VwsEnvironment = {
    url: '/admin',
    id: 'vws_dashboard',
    name: 'Admin & Licences',
    icon: 'settings',
    order: 5,
    target: '_self'
};

export const VWS_DOWNLOAD: VwsEnvironment = {
    url: '/documentation/index.php/download-packages-and-apks',
    id: 'vws_download',
    name: 'Download',
    icon: 'download',
    order: 6,
    target: '_blank'
};

export const VWS_RELEASE_NOTE: VwsEnvironment = {
    url: 'https://smartviser-vws.com/documentation/index.php/release-notes/',
    id: 'vws_release_note',
    name: 'Release Note',
    icon: 'event_note',
    order: 7,
    target: '_blank'
};

export const VWS_DASHBOARD: VwsEnvironment = {
    url: '/dashboard',
    id: 'vws_first_page',
    name: 'Dashboard',
    icon: 'dashboard',
    order: 8,
    target: '_self'
};

export const VWS_USAGE_KPI: VwsEnvironment = {
    url: '/',
    id: 'vws_usage_kpi',
    name: 'Usage kpi',
    icon: 'bar_chart',
    order: 9,
    target: '_self'
};

export const VWS_REMOTE_CONTROL_V2: VwsEnvironment = {
    url: '/remote-control-v2',
    id: 'vws_remote_control_v2',
    name: 'Remote control Neo',
    icon: 'settings_remote',
    order: 10,
    target: '_self'
};

export const VWSServicesConstants: VwsEnvironment[] = 
    [VWS_REPORT_ANALYSIS_V2, VWS_REMOTE_CONTROL, VWS_REMOTE_CONTROL_V2];

export const VWSCommonService: VwsEnvironment[] =
    [VWS_SUPPORT, VWS_ADMIN, VWS_RELEASE_NOTE, VWS_DOCUMENTATION, VWS_DOWNLOAD];
