const coreBaseUrl: string = '/core/v2';
const core: any = {
    roles: `${coreBaseUrl}/roles`,
    messages: `${coreBaseUrl}/messages`,
    modifications: `${coreBaseUrl}/modifications`,
    notifications: `${coreBaseUrl}/notifications`,
    services: `${coreBaseUrl}/services`
};

export const ApiConstants: any = {
    core
};
