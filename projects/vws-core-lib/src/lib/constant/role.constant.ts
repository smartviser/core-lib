export const RoleConstants: any = {
    ADMIN: 'admin',
    ADMIN_SV: 'adminSV',
    ADMIN_SYSTEM: 'adminSystem',
    DEMO: 'demo',
    USER: 'user',
    USER_SV: 'userSV',
    GROUPS: {
        CUSTOMER_ROLES: ['admin', 'user'],
        SV_ADMINS: ['adminSystem', 'adminSV'],
        ADMINS: ['adminSystem', 'adminSV', 'admin'],
        SV_ROLES: ['adminSystem', 'adminSV', 'userSV']
    }
};

export const RoleConstantsId: any = {
    ADMIN: 3,
    ADMIN_SV: 2,
    ADMIN_SYSTEM: 1,
    DEMO: 6,
    USER: 5,
    USER_SV: 4
};


export const RoleGroupConstantsId: any = {
    CUSTOMER_ROLES: [3, 5],
    SV_ADMINS: [1, 2],
    ADMINS: [1, 2, 3],
    SV_ROLES: [1, 2, 4]
};


export const ArrayOrderRoles: string[] = ['AdminSystem', 'AdminSV', 'Admin', 'UserSV', 'User', 'Demo'];
