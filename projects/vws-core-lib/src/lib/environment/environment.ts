import {Injectable} from '@angular/core';

//overridden by calling module at runtime
@Injectable()
export class EnvironmentConfig {
    commonBaseUrl: string = '/api';
    specificBaseUrl: string = '/api';
    baseUrl: string = '';
    base_url: string = '';
    serviceName: string = '';
    production: boolean = false;
}
