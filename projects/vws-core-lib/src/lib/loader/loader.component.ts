import { Component, Input } from '@angular/core';

@Component({
    selector: 'vws-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.less']
})
export class LoaderComponent {
    @Input() loading: boolean|number = false;
}
