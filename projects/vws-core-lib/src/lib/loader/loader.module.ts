import { ModuleWithProviders, NgModule } from '@angular/core';
import { LoaderComponent } from './loader.component';
import { MatProgressBarModule} from '@angular/material/progress-bar'; 
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { EnvironmentConfig } from '../environment/environment';

@NgModule({
    declarations: [LoaderComponent],
    imports: [
        MatProgressBarModule,
        CommonModule,
        BrowserModule
    ],
    providers: [],
    bootstrap: [LoaderComponent],
    exports: [LoaderComponent]
})

export class LoaderModule { 
    static forRoot(configuration: EnvironmentConfig): ModuleWithProviders<LoaderModule> {
        return {
            ngModule: LoaderModule,
            providers: [{provide: EnvironmentConfig, useValue: configuration}]
        };
    }
}

