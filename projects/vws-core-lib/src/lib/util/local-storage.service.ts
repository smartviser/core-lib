import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    getVWSServices(): unknown[] {
        const services: any = this.getLocalStorageItemByName('ls.user_services');
        return services ? services : [];
    }

    setLocalStorageByName(key: string, resource: unknown): void {
        localStorage.setItem(key, JSON.stringify(resource));
    }

    removeLocalStorageItem(key: string): void {
        localStorage.removeItem(key);
    }

    private getLocalStorageItemByName(name: string): Record<string, unknown> | null {
        const item: string = localStorage.getItem(name);
        return item === null ? null : JSON.parse(item);
    }
}
