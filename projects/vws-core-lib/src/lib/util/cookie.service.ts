import { Inject, Injectable } from '@angular/core';
import { User } from '../model/user.interface';
import { CookieService as NgCookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class CookieService { 
    constructor(private ngcookieService: NgCookieService){}

    getUserConnected = (): User | null => 
        this.ngcookieService.get('userConnected') ?
            JSON.parse(decodeURI(this.ngcookieService.get('userConnected'))) as User : null;

    disconnectUser = (): void => this.ngcookieService.delete('userConnected', '/');

    connectUser = (user: User): void => this.ngcookieService.set(
        'userConnected', 
        JSON.stringify(user), 
        new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000),
        '/'
    );
}
