import { Injectable } from '@angular/core';
import { loadAsync, JSZipObject } from 'jszip';
import * as JSZip from 'jszip';
import { saveAs } from 'file-saver';


export const mimeTypes: Record<string, string> = {
    mp4: 'video/mp4',
    csv: 'text/csv',
    zip: 'application/zip',
    json: 'application/json',
    xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    xls: 'application/vnd.ms-excel'
};


@Injectable({
    providedIn: 'root'
})
export class FileService {
    public async readJsonFiles(files: File[]): Promise<any> {
        try {
            const filesJson: any[] = [];
            for (const f of files) {
                if(f.type === mimeTypes['json']){
                    filesJson.push(await this.readJsonFile(f));
                }
            }
            return filesJson.reduce((acc: any, val: any): any => ({...acc, ...val})) as FileList;
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    /**
     * Transforms unusable FileList to File[]
     * 
     * @param f 
     */
    fileListAsFileArray = (f: FileList): File[] => {
        const files: File[] = [];
        let i: number = 0;
        // stupid loop because FileList is not Iterable
        while (i < f.length) {
            files.push(f[i]);
            i++;
        }
        return files;
    };

    /**
     * Unzip a file and get all the files contained as a FileList
     * 
     * @param zipFile 
     */
    getZippedFiles = async (zipFile: File): Promise<File[]> => {
        const promise: ArrayBuffer = await zipFile.arrayBuffer();
        const promiseList$: Array<Promise<File>> = [];

        return new Promise((resolve: (value: File[]) => void, reject: any): any => {
            loadAsync(promise).then(async (fl: JSZip): Promise<void> => {
                const filesList: File[] = [];
                fl.forEach((relativePath: string, f: JSZipObject): any => promiseList$.push(this._jsZipObjectToFile(f)));
                [...await Promise.all(promiseList$)].forEach((f: File): any => {
                    filesList.push(f);
                });
                resolve(filesList);
            }, (e: any): any => {
                reject(e);
            });
        });
    };

    saveZipFile(response: BlobPart, name: string): void{
        const blob: Blob = new Blob([response], {
            type: 'application/zip'
        });
        const fileOfBlob: File = new File([blob], `${name}.zip`);
        saveAs(fileOfBlob);
    }

    saveJsonFile(response: BlobPart, name: string): void{
        const blob: Blob = new Blob([response], {
            type: 'application/json'
        });
        const fileOfBlob: File = new File([blob], `${name}.json`);
        saveAs(fileOfBlob);
    }
    
    public async readJsonFile(file: File): Promise<any> {
        try {
            const txtContent: string = await this.readUploadedFileAsText(file);
            return JSON.parse(txtContent);
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    private readUploadedFileAsText(inputFile: File): Promise<string> {
        const fileReader: FileReader = new FileReader();

        return new Promise((resolve: any, reject: any): void => {
            fileReader.onerror = (): any => {
                fileReader.abort();
                reject(new DOMException('Problem parsing input file.'));
            };

            fileReader.onloadend = (): any => {
                resolve(fileReader.result);
            };

            fileReader.readAsText(inputFile);
        });
    }

    /**
     * Transforms a JsZipObject to JS File
     * 
     * @param jsZ 
     */
    private _jsZipObjectToFile = async (jsZ: JSZipObject): Promise<File> => {
        const filename: string = jsZ.name;
        const zblob: Blob = await jsZ.async('blob');
        return new File([zblob], filename, {
            type: this._getTypeFromExtension(filename)
        });
    };

    /**
     * Since blob conversion loses mimeType, we have to rely on this
     * 
     * @param fileName 
     */
    private _getTypeFromExtension = (fileName: string): string => {
        const extension: string = fileName.slice((fileName.lastIndexOf('.') - 1 >>> 0) + 2);
        return mimeTypes[extension] || null;
    };
}

