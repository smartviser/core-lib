
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user.interface';
import { CookieService } from './cookie.service';

@Injectable({
    providedIn: 'root'
})

export class ApiService {

    private token: string;

    constructor(private cookieService: CookieService) {
        const userConnected: User = this.cookieService.getUserConnected();
        if(userConnected){
            this.token = userConnected.token;
        }
    }

    public generateAuthHeaderV1(): {headers: HttpHeaders}{
        return {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'X-AUTH-TOKEN': this.token
            })
        };
    }

    public generateAuthHeaderV1File(): {headers: HttpHeaders}{
        return {
            headers: new HttpHeaders({
                'Accept': 'application/json, text/plain, */*',
                'X-AUTH-TOKEN': this.token
            })
        };
    }

    public generateAuthHeaderV2(): {headers: HttpHeaders}{
        return {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'authentication': this.token
            })
        };
    }
}
