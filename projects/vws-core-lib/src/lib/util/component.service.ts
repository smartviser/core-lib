import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ComponentService {
    checkRequiredParameter(parameter: any, parameterName: string): void {
        if (parameter === null) {
            throw new Error(`Attribute ${parameterName} is required`);
        }
    }
}
