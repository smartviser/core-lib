import { Observable, catchError, from, map, tap } from 'rxjs';
import { CookieService } from './cookie.service';
import { SecurityService } from './security.service';
import { EnvironmentConfig } from '../environment/environment';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function initializeAppFactory(cookieService: CookieService, securityService: SecurityService, 
                                     environmentConfig: EnvironmentConfig): () => Observable<boolean> {
    const userConnected: any = cookieService.getUserConnected();
    if (!userConnected) {
        window.location.href = '/login';
    }
    return (): Observable<any> => securityService.hasAccessToThisService(environmentConfig.serviceName).pipe(
        tap((hasAccess: boolean): any => {
            if(!hasAccess){
                window.location.href = '/';
                // Throw error to redirect stop application
                throw new Error('Unauthorized');
            }
        }), catchError((): any => { 
            window.location.href = '/';
            // Throw error to redirect stop application
            throw new Error('Unauthorized');
        }
        )
    );
}
