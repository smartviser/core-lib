import {Injectable} from '@angular/core';
import {VWSService} from '../model/vws-service.interface';
import { VwsEnvironment } from '../model/vws-environment.interface';
import { VWSCommonService, VWSServicesConstants } from '../constant/vws-services.constant';
import { VWSServicesService } from '../api/vws-services.service';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SecurityService {

    userServices: VwsEnvironment[] = [];

    constructor(private vwsServicesService: VWSServicesService) {
    }

    hasAccessToThisService(serviceName: string): Observable<boolean> {
        return this.vwsServicesService.findUserServices().pipe(
            map((vwsServiceUser: VWSService[]): boolean => vwsServiceUser.some((s: any): boolean => s.name === serviceName)
            ));
    }

    getUserService(serviceParentName: VwsEnvironment): Observable<VwsEnvironment[]> {
        return this.vwsServicesService.findUserServices().pipe(
            map((services: VWSService[]): VwsEnvironment[] => this.findUserServiceById(services, serviceParentName)
            ));
    }

    findUserServiceById(vwsServices: any[], parentService: VwsEnvironment): VwsEnvironment[] {
        let userServices: any[] = VWSServicesConstants.filter((serviceConstant: VwsEnvironment): boolean =>
            vwsServices.some((s: any): boolean => s.name === serviceConstant.id && serviceConstant.id !== parentService.id));
        // get its constant value from VWSCommonService (except the current service)
        const commonServices: any[] = VWSCommonService.filter((serviceConstant: VwsEnvironment): boolean =>
            serviceConstant.id !== parentService.id);
        // set default services
        userServices = userServices.concat(commonServices);
        
        return userServices.sort((a: any, b: any): number => a.order - b.order);
    }

}
