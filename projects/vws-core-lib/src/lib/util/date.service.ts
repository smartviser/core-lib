import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DateService {

    private readonly locale: string = 'en-GB';

    getDateString(timestamp: number): string {
        const date: Date = new Date(timestamp);
        return date.toLocaleDateString(this.locale);
    }

    getDateTimeString(timestamp: number): string {
        const date: Date = new Date(timestamp);
        return `${date.toLocaleDateString(this.locale)} ${date.toLocaleTimeString(this.locale)}`;
    }
}
