export interface VwsEnvironment {
    url: string;
    id: string;
    name: string;
    icon: string;
    order: number;
    target: string;
}
