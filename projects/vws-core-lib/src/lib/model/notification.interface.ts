export interface Notification {
    id: number;
    icon: string;
    message: string;
    date: number;
    stringDate: string;
    isNew: boolean;
    sender: number;
    url: string;
}
