export interface VWSService {
    id: number;
    name: string;
    description: string;
}
