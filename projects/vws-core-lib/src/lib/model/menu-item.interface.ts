export interface MenuItem {
    label: string;
    link: string;
    icon?: string;
    selected: boolean;
}
