export interface User {
    id: number;
    name: string;
    surname: string;
    email: string;
    token: string;
    roles: number;
    customer: number;
    services: number[];
    username: string;
}
